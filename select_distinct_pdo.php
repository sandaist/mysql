<?php

/*
  Select distinct records
*/
function select_distinct_pdo(PDO $pdoDatabase){

  try{
    $stmt = $pdoDatabase->prepare("
      SELECT DISTINCT (`name`)  FROM `domains`;
    ");
    $stmt -> execute();
    $result = $stmt -> fetchAll();
    var_dump($result);
  }

  catch(PDOException $e){
    echo "Error:".$e->getMessage();
  }

}
