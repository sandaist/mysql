<?php
/*
Insert multiple records
*/
function insert_one_record_pdo(PDO $pdoDatabase, $name, $type, $created_at, $price)
{
  try {

    $stmt = $pdoDatabase->prepare("INSERT INTO domains (name, type, created_at, price) VALUES (:name, :type, :created_at, :price)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':created_at', $created_at);
    $stmt->bindParam(':price', $price);

    $stmt->execute();
  }
  catch(PDOEXception $e){
    echo "Error:" . $e->getMessage ;
  }
}

//insert one record
function insert_array_records_pdo(PDO $pdoDatabase, array $data_records)
{
  for ($i=0, $l = count($data_records); $i < $l; $i++)
  {
    insert_one_record_pdo($pdoDatabase, $data_records[$i]['name'], $data_records[$i]['type'], $data_records[$i]['created_at'], $data_records[$i]['price']);
  }
}
