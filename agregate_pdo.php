<?php
/*
Data agregation
 */

 function agregate_pdo(PDO $pdoDatabase){

   try{
     //сумма
     $stmt = $pdoDatabase ->prepare("
      SELECT SUM(`price`) FROM `domains`;
     ");
     $stmt -> execute();
     $result = $stmt->fetchColumn();
     echo "Summ is:".$result. "<br>";

     //среднее цена
     $stmt = $pdoDatabase ->prepare("
      SELECT AVG(`price`) FROM `domains`;
     ");
     $stmt -> execute();
     $result = $stmt->fetchColumn();
     echo "Average is:".$result. "<br>";

     //колиство доменов
     $stmt = $pdoDatabase ->prepare("
      SELECT COUNT(*) FROM `domains`;
     ");
     $stmt -> execute();
     $result = $stmt->fetchColumn();
     echo "Amount is:".$result. "<br>";

     //min
     $stmt = $pdoDatabase ->prepare("
      SELECT MIN(`price`) FROM `domains`;
     ");
     $stmt -> execute();
     $result = $stmt->fetchColumn();
     echo "Minimum price is:".$result. "<br>";

     //max
     $stmt = $pdoDatabase ->prepare("
      SELECT MAX(`price`) FROM `domains`;
     ");
     $stmt -> execute();
     $result = $stmt->fetchColumn();
     echo "Maximum price is:".$result. "<br>";

   }
   catch(PDOEXception $e){
     echo "Error:".$e->getMessage();
   }
 }
