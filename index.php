<?php

include_once 'create_table_pdo.php';
include_once 'insert_array_records_pdo.php';
include_once 'update_record_pdo.php';
include_once 'delete_record_pdo.php';
include_once 'agregate_pdo.php';
include_once 'select_distinct_pdo.php';
include_once 'add_column_pdo.php';
include_once 'delete_table_pdo.php';

$pdoDatabase = new PDO ('mysql:host=localhost;dbname=test;charset=utf8', 'root', '12345');

$name = ".com";
$type = "2 level";
$created_at = "2016-01-01";
$price = 100;


$data_records = array (

  array(
    "name" => "sample",
    "type" => "3 level",
    "created_at" => "2015-01-02",
    "price" => 300
  ),

  array(
    "name" => "test",
    "type" => "2 level",
    "created_at" => "2017-05-04",
    "price" => 100
  ),

  array(
    "name" => "test3",
    "type" => "1 level",
    "created_at" => "2017-05-02",
    "price" => 550
  ),

  array(
    "name" => "test2",
    "type" => "4 level",
    "created_at" => "2014-05-03",
    "price" => 450
  ),
  array(
    "name" => "test1",
    "type" => "5 level",
    "created_at" => "2017-03-03",
    "price" => 350
  ),

);

create_table_pdo($pdoDatabase);
insert_array_records_pdo($pdoDatabase, $data_records);
insert_one_record_pdo($pdoDatabase, $name, $type, $created_at, $price);
update_record_pdo($pdoDatabase);
delete_record_pdo($pdoDatabase);
agregate_pdo($pdoDatabase);
select_distinct_pdo($pdoDatabase);
add_column_pdo($pdoDatabase);
delete_table_pdo($pdoDatabase);
