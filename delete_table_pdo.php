<?php

/*
  Delete table
*/
function delete_table_pdo(PDO $pdoDatabase){

  try{
    $stmt = $pdoDatabase->prepare("
      DROP TABLE IF EXISTS `newdomains`;
    ");
    $stmt -> execute();
  }

  catch(PDOException $e){
    echo "Error:".$e->getMessage();
  }

}
