<?php
/*
Create table
*/

function create_table_pdo(PDO $pdoDatabase) {

  try {
      $stmt= $pdoDatabase->prepare("CREATE TABLE IF NOT EXISTS `newdomains` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `type` varchar(255) NOT NULL,
        `created_at` date,
        `price` decimal(5,2) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
      ");
      $stmt->execute();
  }

  catch (PDOException $e){
    echo "Error:" . $e->getMessage ;
  }
}
