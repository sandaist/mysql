<?php

/*
  Delete records by condition
*/
function delete_record_pdo(PDO $pdoDatabase){

  try{
    $stmt = $pdoDatabase->prepare("
      DELETE FROM `domains`
      WHERE `price` < 200;
    ");

    $stmt -> execute();
  }

  catch(PDOException $e){
    echo "Error:".$e->getMessage();
  }

}
