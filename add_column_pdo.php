<?php

/*
  Add column
*/
function add_column_pdo(PDO $pdoDatabase){

  try{
    $stmt = $pdoDatabase->prepare("
      ALTER TABLE `domains` ADD `comment` VARCHAR(255);
    ");
    $stmt -> execute();
  }

  catch(PDOException $e){
    echo "Error:".$e->getMessage();
  }

}
